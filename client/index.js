import './index.scss';
import onChange from 'on-change';
import { createEl, appendChildren, generateData } from './modules/utils';
import Graph from './modules/graph';
import Graph2 from './modules/graph2';
import Draw from './modules/draw';
import { dataShape } from './config.json';

const sameDate = (a, b) => a.getYear() === b.getYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();

class App {
  constructor() {
    this.update = this.update.bind(this);

    const state = {
      data: generateData(dataShape, 5000),
      drawing: false,
      line: [],
    };
    this.state = onChange(state, this.update); // monitor state changes

    this.element = createEl('div', { className: 'app' });

    this.graph = new Graph(this.state);
    this.graph2 = new Graph2(this.state);

    this.draw = new Draw(this.state);

    appendChildren(this.element, this.graph.element, this.graph2.element, this.draw.element);

    fetch('assets/data/owid-covid-data.json').then(res => res.json()).then((countryData) => {
      this.firstDate = new Date();
      this.lastDate = new Date('2000-01-01');
      this.lowestCases = Number.POSITIVE_INFINITY;
      this.highestCases = 0;
      this.allDates = [];
      // const stopDate = new Date('2020-05-10');
      const newData = [];
      Object.keys(countryData).forEach((country, index) => {
        const { data } = countryData[country];
        const dates = [];
        data.forEach((datum) => {
          const parts = datum.date.split('-');
          datum.date = new Date(parts[0], parts[1] - 1, parts[2]);
          dates.push(datum.new_cases_per_million);
          // if (datum.date < stopDate) {
          let unique = true;
          for (let i = 0; i < this.allDates.length; i += 1) {
            if (sameDate(datum.date, this.allDates[i])) {
              unique = false;
              i = this.allDates.length;
            }

            if (datum.new_cases_per_million < this.lowestCases) this.lowestCases = datum.new_cases_per_million;
            if (datum.new_cases_per_million > this.highestCases) this.highestCases = datum.new_cases_per_million;
          }
          if (unique) this.allDates.push(datum.date);
          if (datum.date < this.firstDate) this.firstDate = datum.date;
          if (datum.date > this.lastDate) this.lastDate = datum.date;
          // }
        });
        newData.push({ name: country, dates });
      });

      // Object.keys(countryData).forEach((country) => {
      //   countryData[country].data.forEach((date) => {
      //     console.log(country, date);
      //   });
      // });

      console.log(newData);

      this.graph2.resize();
      this.graph2.render(countryData, this.allDates, 3500, this.highestCases);
    });
  }


  // state has changed, decide what to update in here:
  update(path, current, previous) {
    if (path === 'line' || path === 'lineSimplified' || path === 'data') return;
    console.log('state change:', path, ':', previous, '->', current);
    if (path === 'drawing' && !current) {
      // this.graph.match();
    }
  }
}

const app = new App();
document.body.appendChild(app.element);
app.draw.resize();
app.graph.resize();
app.graph2.resize();

app.graph.render();
window.app = app; // For inspecting in browser, remove before push to production
