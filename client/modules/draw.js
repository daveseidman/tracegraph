import { createEl } from './utils';

export default class Draw {
  constructor(state) {
    this.state = state;
    this.mousedown = this.mousedown.bind(this);
    this.mouseup = this.mouseup.bind(this);
    this.mousemove = this.mousemove.bind(this);
    this.state.line = [];

    this.element = createEl('div', { className: 'draw' }, {}, { mousedown: this.mousedown, mousemove: this.mousemove, mouseup: this.mouseup });

    this.canvas = createEl('canvas', { className: 'draw-canvas' });
    this.context = this.canvas.getContext('2d');
    this.element.appendChild(this.canvas);
  }

  mousedown() {
    this.state.drawing = true;
    this.state.line = [];
  }

  mouseup() {
    this.state.drawing = false;
  }

  mousemove({ offsetX, offsetY }) {
    if (this.state.drawing) {
      this.state.line.push({ x: offsetX, y: offsetY });
      this.render();
    }
  }

  render() {
    this.context.clearRect(0, 0, this.width, this.height);
    this.context.beginPath();
    this.context.moveTo(this.state.line[0].x, this.state.line[0].y);
    for (let i = 1; i < this.state.line.length; i += 1) {
      this.context.lineTo(this.state.line[i].x, this.state.line[i].y);
    }
    this.context.stroke();
  }

  resize() {
    const { width, height } = this.element.getBoundingClientRect();
    this.width = width;
    this.height = height;
    this.canvas.width = width;
    this.canvas.height = height;
  }
}
