import { createEl } from './utils';
import { dataShape } from '../config.json';

const matchAmount = 3;

export default class Graph {
  constructor(state) {
    this.state = state;

    this.element = createEl('div', { className: 'graph' });

    this.canvas = createEl('canvas', { className: 'graph-canvas' });
    this.context = this.canvas.getContext('2d');
    this.element.appendChild(this.canvas);

    this.width = 0;
    this.height = 0;
  }

  simplifyLine() {
    const { amount } = dataShape.improvement.values;
    this.state.lineSimplified = [];
    for (let i = 0; i < amount; i += 1) {
      const xTarget = i * (this.width / amount);
      const closest = {
        value: Number.POSITIVE_INFINITY,
        index: null,
      };
      for (let j = 0, { length } = this.state.line; j < length; j += 1) {
        const distance = Math.abs(this.state.line[j].x - xTarget);
        if (distance < closest.value) {
          closest.value = distance;
          closest.index = j;
        }
      }
      const { y } = this.state.line[closest.index];
      this.state.lineSimplified.push({ x: xTarget, y });
    }

    this.context.strokeStyle = 'rgba(255, 0, 0, 1)';
    this.context.beginPath();
    this.context.moveTo(this.state.lineSimplified[0].x, this.state.lineSimplified[0].y);
    for (let k = 1; k < this.state.lineSimplified.length; k += 1) {
      this.context.lineTo(this.state.lineSimplified[k].x, this.state.lineSimplified[k].y);
    }
    this.context.stroke();
  }

  findSimilar() {
    this.state.data.forEach((datum) => {
      const { improvement } = datum;
      datum.difference = 0;
      improvement.forEach((value, index) => {
        datum.difference += Math.abs((value / 100) - (this.state.lineSimplified[index].y / this.height));
      });
    });
    this.state.data.sort((a, b) => (a.difference > b.difference ? 1 : -1));
  }

  showSimilar() {
    this.state.data.forEach((datum, index) => {
      if (index > matchAmount) return;
      const { improvement } = datum;
      this.context.strokeStyle = 'rgba(0, 0, 0, .75)';
      this.context.lineWidth = 2;
      this.context.beginPath();
      for (let i = 0, { length } = improvement; i < improvement.length; i += 1) {
        const x = i * (this.width / length);
        const y = (improvement[i] / 100) * this.height;
        this.context[i === 0 ? 'moveTo' : 'lineTo'](x, y);
      }
      this.context.stroke();
    });
  }

  match() {
    this.render();
    this.simplifyLine();
    this.findSimilar();
    this.showSimilar();
  }

  render() {
    this.context.clearRect(0, 0, this.width, this.height);
    this.state.data.forEach(({ improvement }) => {
      this.context.lineWidth = 0.5;
      this.context.strokeStyle = 'rgba(0, 0, 0, 0.05)';
      this.context.beginPath();
      for (let i = 0, { length } = improvement; i < improvement.length; i += 1) {
        const x = i * (this.width / length);
        const y = (improvement[i] / 100) * this.height;
        this.context[i === 0 ? 'moveTo' : 'lineTo'](x, y);
      }
      this.context.stroke();
    });
  }

  resize() {
    const { width, height } = this.element.getBoundingClientRect();
    this.width = width;
    this.height = height;
    this.canvas.width = width;
    this.canvas.height = height;
  }
}
