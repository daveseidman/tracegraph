// Collection of utility functions used throughout the app

module.exports.Mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
module.exports.Android = /Android/.test(navigator.userAgent);

module.exports.createEl = (type, options, attributes, events) => {
  const el = document.createElement(type);

  if (options) Object.keys(options).forEach((key) => {
    el[key] = options[key];
  });

  if (attributes) Object.keys(attributes).forEach((key) => {
    el.setAttribute(key, attributes[key]);
  });

  if (events) Object.keys(events).forEach((key) => {
    el.addEventListener(key, events[key]);
  });

  if (type === 'select' && options.values) {
    if (options.placeholder) {
      const label = document.createElement('option');
      label.innerText = options.placeholder;
      label.value = '';
      el.appendChild(label);
    }

    options.values.forEach((value) => {
      const option = document.createElement('option');
      option.innerText = value;
      el.appendChild(option);
    });
  }

  if ((type === 'ul' || type === 'ol') && options.values) {
    options.values.forEach((item) => {
      const li = document.createElement('li');
      li.innerText = item;
      el.appendChild(li);
    });
  }
  return el;
};

module.exports.appendChildren = (parent, ...children) => {
  children.forEach((child) => {
    parent.appendChild(child);
  });
};

module.exports.generateData = (shape, length) => {
  const data = [];
  for (let i = 0; i < length; i += 1) {
    const datum = {};
    Object.keys(shape).forEach((key) => {
      const { type, values } = shape[key];
      if (type === 'array') {
        datum[key] = [];
        for (let j = 0; j < values.amount; j += 1) {
          datum[key].push((Math.random() * values.max - values.min) + values.min);
        }
      }

      if (type === 'single') {
        datum[key] = values.length
          ? values[Math.floor(Math.random() * values.length)]
          : (Math.random() * values.max - values.min) + values.min;
      }
    });
    data.push(datum);
  }
  return data;
};
