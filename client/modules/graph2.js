import { createEl } from './utils';

const sameDate = (a, b) => a.getYear() === b.getYear() && a.getMonth() === b.getMonth() && a.getDate() === b.getDate();

const randomColor = () => {
  const red = Math.floor(Math.random() * 255);
  const green = Math.floor(Math.random() * 255);
  const blue = Math.floor(Math.random() * 255);

  return `rgba(${red},${green},${blue},.1)`;
};


const formatDate = (date) => {
  const month = date.getMonth() < 10 ? `0${date.getMonth()}` : date.getMonth();
  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
  return `${month}/${day}`;
};
export default class Graph2 {
  constructor(state) {
    this.state = state;

    this.element = createEl('div', { className: 'graph2' });

    this.canvas = createEl('canvas', { className: 'graph2-canvas' });
    this.context = this.canvas.getContext('2d');
    this.element.appendChild(this.canvas);

    this.width = 0;
    this.height = 0;
  }

  render(countryData, allDates, low, high) {
    this.context.clearRect(0, 0, this.width, this.height);
    this.context.lineWidth = '1px';
    this.context.strokeStyle = 'rgba(0, 0, 0, 1)';
    this.context.font = '16px sans-serif';
    let j = 0;
    // draw date gridlines
    for (let x = 0; x <= this.width * allDates.length; x += (this.width / allDates.length)) {
      this.context.beginPath();
      this.context.moveTo(x, 0);
      this.context.lineTo(x, this.height - 20);
      this.context.stroke();
      if ((j % 10 === 0 || j === 0) && allDates[j]) this.context.fillText(formatDate(allDates[j]), x, this.height);
      j += 1;
    }

    // draw covid positive gridlines
    for (let y = 0; y < this.height * 10; y += (this.height / 10)) {
      this.context.beginPath();
      this.context.moveTo(0, y);
      this.context.lineTo(this.width, y);
      this.context.stroke();
    }

    // console.log(countryData);
    Object.keys(countryData).forEach((country, countryIndex) => {
      // if (index > 10) return;
      const { data } = countryData[country];
      // console.log(data);
      // console.log(data);
      // console.log(data);
      this.context.lineWidth = 2;

      this.context.beginPath();
      this.context.strokeStyle = randomColor();
      // this.context.moveTo(0, 0);
      let lines = 0;

      data.forEach((timepoint, index) => {
        const { date, new_cases_per_million } = timepoint;
        // let x = 0;
        // for (let i = 0; i < allDates.length; i += 1) {
        //   if (sameDate(allDates[i], date)) {
        //     x = i * (this.width / allDates.length);
        //   }
        // }
        const x = index * (this.width / allDates.length);
        const y = this.height * ((new_cases_per_million) / (high - low));

        this.context[index === 0 ? 'moveTo' : 'lineTo'](x, y);
        this.context.stroke();
        lines += 1;
      });
    });

    // this.state.data.forEach(({ improvement }) => {
    //   this.context.lineWidth = 0.5;
    //   this.context.strokeStyle = 'rgba(0, 0, 0, 0.05)';
    //   this.context.beginPath();
    //   for (let i = 0, { length } = improvement; i < improvement.length; i += 1) {
    //     const x = i * (this.width / length);
    //     const y = (improvement[i] / 100) * this.height;
    //     this.context[i === 0 ? 'moveTo' : 'lineTo'](x, y);
    //   }
    //   this.context.stroke();
    // });
  }

  resize() {
    const { width, height } = this.element.getBoundingClientRect();
    this.width = width * 2;
    this.height = height * 2;
    this.canvas.width = this.width;
    this.canvas.height = this.height;
  }
}
